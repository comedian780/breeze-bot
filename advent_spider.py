#!/usr/bin/env python

import os
import re
import datetime
import scrapy
import requests
from scrapy.crawler import CrawlerProcess


class BreezeAdventSpider(scrapy.Spider):
    name = 'breezeadventspider'
    start_urls = [os.getenv('ADVENT_URL', 'https://summer-breeze.de/de/')]

    def send_telegram_message(self, message):
        url = "https://api.telegram.org/bot" + os.getenv('BOT_ID') + \
            "/sendMessage?chat_id=" + os.getenv('CHAT_ID') + "&text=" + message
        requests.get(url)

    def parse(self, response):
        day_of_month = str(datetime.datetime.now().day)
        cur_cal_id = "i"

        for entry in response.css('.day a.cover').getall():
            if re.search('>(.+?)<', entry).group(1) == day_of_month:
                cur_cal_id += re.search('id="(.+?)"', entry).group(1)
                break

        messages_sent = 0
        for entry in response.css('ul[id=' + cur_cal_id + '] '
                '.swiper-slide a::attr(href)').getall():
            if messages_sent < 1:
                if '/de/' in os.getenv('ADVENT_URL'):
                    self.send_telegram_message("Heutige Neuankündigungen:")
                else:
                    self.send_telegram_message("Todays new announcements:")
            self.send_telegram_message(entry)
            messages_sent += 1

        return {"entries": False}
