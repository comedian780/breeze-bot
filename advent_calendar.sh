#!/bin/sh

read_env(){
  set -a
  [ -f .env ] && . .env
  set +a
}

read_env
scrapy runspider advent_spider.py
