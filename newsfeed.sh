#!/bin/sh


read_env(){
  set -a
  [ -f .env ] && . .env
  set +a
}

write_env(){
  # can't change inode of .env -> workaround with temp file
  cp .env .env.temp
  sed -i "/${1}/c${1}=\"${2}\"" .env.temp
  cat .env.temp > .env
}

send_telegram_message_to_channel(){
  curl "https://api.telegram.org/bot${1}/sendMessage?chat_id=${2}&text=${3}"
}

read_env

latest_update=$(scrapy runspider -o - -t json spider.py | jq -r .[].latest_post)

if [[ "$OLD_POST" != "$latest_update" ]]; then
  send_telegram_message_to_channel $BOT_ID $CHAT_ID $latest_update
  write_env OLD_POST $latest_update
fi
