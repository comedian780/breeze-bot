FROM python:alpine

WORKDIR /bot
COPY . .

RUN cat dependencies | xargs apk add --no-cache && \
  cat build_dependencies | xargs apk add --no-cache && \
  pip install -qr requirements.txt && \
  cat build_dependencies | xargs apk del
ENTRYPOINT ["ash"]
CMD ["newsfeed.sh"]
