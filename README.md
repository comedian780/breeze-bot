[![pipeline status](https://gitlab.com/comedian780/breeze-bot/badges/master/pipeline.svg)](https://gitlab.com/comedian780/breeze-bot/commits/master)

## breeze-bot

Telegram bot that uses scrapy to crawl a website (originally the homepage of the summer breeze open air) for news and publishes new entries to a desired telegram channel or conversation.  
For continuous updates use a cron job.

### Prerequisites:

-   a Telegram bot (used as sender)
-   a Telegram channel or a conversation with the bot (used as target)


### Usage:

-   first set the necessary parameters in a .env file
-   then run the container:

```bash
echo 'CHAT_ID="@channel_name"' > .env
echo 'BOT_ID="your:bot-id"' >> .env
echo 'OLD_POST="https://www.summer-breeze.de/de/news/asdf"' >> .env
echo 'SUBSCRIBE_URL="https://summer-breeze.de/de/news/"' >> .env
echo 'SCRAPY_CSS_SELECTOR="article a::attr(href)"' >> .env

docker run -d --rm -v ${PWD}/.env:/bot/.env registry.gitlab.com/comedian780/breeze-bot
docker run -d --rm -v ${PWD}/.env:/bot/.env registry.gitlab.com/comedian780/breeze-bot advent_calendar.sh
```

### Automated updates

```bash
# crontab entry
*/5 * * * * docker run -d --rm -v /path/to/.env:/bot/.env --name=notifier registry.gitlab.com/comedian780/breeze-bot > /dev/null
00 10 1-24 12 * docker run -d --rm -v /path/to/.env:/bot/.env --name=advent registry.gitlab.com/comedian780/breeze-bot advent_calendar.sh > /dev/null
```
